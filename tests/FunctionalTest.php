<?php
namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class FunctionalTest extends WebTestCase
{
public function testShouldDisplayClientIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client index');
    }

    public function testShouldDisplayLocationIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location index');
    }

    public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }

    // ... Ajoutez des tests similaires pour les pages de création (new) et d'ajout (edit)

    public function testShouldAddNewClient()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        // Remplacez les champs avec les données spécifiques à votre entité
        $form = $buttonCrawlerNode->form([
            'client[cin]'    => '123456789',
            'client[nom]'    => 'John',
            'client[prenom]' => 'Doe',
            'client[adresse]' => '123 Main St',
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'John Doe');
    }

public function testShouldAddNewVoiture()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/voiture/new');

    $buttonCrawlerNode = $crawler->selectButton('Save');

    $form = $buttonCrawlerNode->form();

    // Remplacez les champs avec les données spécifiques à votre entité Voiture
    $form = $buttonCrawlerNode->form([
        'voiture[serie]' => 'ABC123',
        'voiture[dateMiseEnMarche][year]'  => '2023',
        'voiture[dateMiseEnMarche][month]' => '01',
        'voiture[dateMiseEnMarche][day]'   => '01',
        'voiture[modele]' => 'Corolla',
        'voiture[prixJour]' => '50',
        // Assurez-vous de remplacer ces champs avec ceux de votre entité Voiture
    ]);

    $client->submit($form);

    $this->assertResponseIsSuccessful();

    // Ajoutez une assertion pour vérifier le texte de la page après l'ajout d'une nouvelle voiture
    $this->assertSelectorTextContains('body', 'Votre texte attendu après l\'ajout d\'une nouvelle voiture');
}


public function testShouldAddNewLocation()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/location/new');

    $buttonCrawlerNode = $crawler->selectButton('Save');

    $form = $buttonCrawlerNode->form();

    // Remplacez les champs avec les données spécifiques à votre entité Location
    $form->setValues([
        'location[dateDebut]' => [
            'year' => '2023',
            'month' => '01',
            'day' => '01',
        ],
        'location[dateRetour]' => [
            'year' => '2023',
            'month' => '01',
            'day' => '10',
        ],
        'location[prix]'       => '500',
        'location[client]'     => '1',
        'location[voiture]'    => 'null',
    ]);

    $client->submit($form);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Location created successfully');
}

}
